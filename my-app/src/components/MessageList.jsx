import React from 'react'
import '../styles/message-list.css';
import Message from './Message.jsx'
import UserActiveMessage from './UserActiveMessage.jsx'
import PropTypes from 'prop-types';

class MessageList extends React.Component {

  createMessage(item, activeUserId, prevMessageDate, onEdit, onDelete) {
    const userAvatar = item.avatar;
    const messageText = item.text;
    const messageDate = item.createdAt;
    const messageId = item.id;

    if (item.userId === activeUserId) {
      return (
        <UserActiveMessage
          text={messageText}
          date={messageDate}
          messageId={messageId}
          onEdit={onEdit}
          onDelete={onDelete}
          prevMessageDate={prevMessageDate}
        />
      )
    }
    else return (
      <Message
        avatar={userAvatar}
        text={messageText}
        date={messageDate}
        messageId={messageId}
        prevMessageDate={prevMessageDate}
      />
    )
  }

  render() {
    const {
      data,
      activeUserId,
      prevMessageDate,
      onEdit,
      onDelete
    } = this.props;

    return (
      <div className="message-list">
        {
          data.map(item => this.createMessage(item, activeUserId, prevMessageDate, onEdit, onDelete))
        }
      </div>
    )
  }
}

MessageList.propTypes = {
  data: PropTypes.array,
  activeUserId: PropTypes.string,
  prevMessageDate: PropTypes.string,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func
};

export default MessageList;