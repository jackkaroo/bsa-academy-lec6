import React from 'react'
import '../styles/message.css';
import { convertTime } from '../helpers/functions.js'
import PropTypes from 'prop-types';

class UserActiveMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenEdit: false
    };
  }
  render() {
    const {
      text,
      date,
      messageId,
      onDelete,
      onEdit
    } = this.props;

    const onOpenEditModal = () => {
      if (!this.state.isOpenEdit) {
        document.getElementById('message_input-button').style.display = 'none';
        document.getElementById('message_edit-button').style.display = 'block';
        document.getElementById('message_edit-button').addEventListener('click', function () { onEdit(messageId) })
        let isOpenEdit = true;
        this.setState({ isOpenEdit })
      } else {
        document.getElementById('message_input-button').style.display = 'block';
        document.getElementById('message_edit-button').style.display = 'none';
        let isOpenEdit = false;
        this.setState({ isOpenEdit })
      }

    }

    return (
      <div className="message_active-wrapper">
        <div className="message_icons">
          <i className="fa fa-pencil-square-o message_item-edit" onClick={onOpenEditModal} aria-hidden="true"></i>
          <i className="fa fa-trash-o message_item-delete" onClick={() => onDelete(messageId)} aria-hidden="true"></i>
        </div>
        <div className="message_item_active">
          <div className="message_item_active-text">{text}</div>
          <div className="message_item_active-date">{convertTime(date)}</div>
        </div>
      </div>
    )
  }
}

UserActiveMessage.propTypes = {
  text: PropTypes.string,
  date: PropTypes.string,
  messageId: PropTypes.string,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func
};

export default UserActiveMessage;