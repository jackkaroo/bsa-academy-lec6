import React from 'react'
import '../styles/header.css';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Need Fix'
    }
  }

  render() {
    return (
      <div className="header_nav">
        <img className="header_nav-icon" src="https://d3ot0t2g92r1ra.cloudfront.net/img/logo@3x_optimized.svg" alt="icon" />
      </div>
    )
  }
}


export default Header;