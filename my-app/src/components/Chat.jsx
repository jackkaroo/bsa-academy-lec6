import React from 'react'
import MessageConfig from './config/MessageConfig.json'
import ChatHeader from './ChatHeader.jsx'
import MessageList from './MessageList.jsx'
import MessageInput from './MessageInput.jsx';
import Header from './Header.jsx';
import Footer from './Footer.jsx';
import { findMessageById } from '../helpers/functions.js'
import { create_UUID } from '../helpers/functions.js'

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      info: MessageConfig,
      activeUser: { "user": "Kateryna", "avatar": "https://pbs.twimg.com/profile_images/3076927289/8d6afe6fe82689ab96da9ede6deeabbf.jpeg", "userId": `${create_UUID}` },
      text: ''
    };
  }

  getUsers() {
    const data = this.state.info;
    const users = data.map(el => el.user)
    const uniqueUsers = [...new Set(users)]
    return uniqueUsers;
  }

  handleSend = () => {
    const info = [...this.state.info];
    const message = this.state.text;
    if (!message) {
      alert('Empty message');
      return;
    }
    const user = this.state.activeUser;
    const date = new Date();
    const newMessageId = create_UUID();
    let newMessage = { "id": `${newMessageId}`, "text": `${message}`, "user": `${user.user}`, "avatar": `${user.avatar}`, "userId": `${user.userId}`, "editedAt": "", "createdAt": `${date}` }
    info.push(newMessage)
    this.setState({ info })
    const text = '';
    this.setState({ text })
  }

  handleEnter = (e) => {
    const value = e.target.value;
    const text = value;
    this.setState({ text });
  }

  handleEdit = (messageId) => {
    const info = [...this.state.info];
    const message = this.state.text;
    if (!message) {
      alert('Empty message');
      return;
    }

    const index = findMessageById(info, messageId);
    console.log(index)
    if (!index) return;
    info[index].text = message;
    this.setState({ info })

    document.getElementsByClassName('message_input-text')[0].value = ''
    document.getElementById('message_edit-button').style.display = 'none';
    document.getElementById('message_input-button').style.display = 'block';
  }

  handleDelete = (messageId) => {
    const data = this.state.info;
    const index = findMessageById(data, messageId);
    if (!index) return;
    data.splice(index, 1);
    this.setState({ data })
  }

  render() {
    const data = this.state.info;
    const activeUserId = this.state.activeUser.userId;
    return (
      <div className="Chat">
        <Header />
        <ChatHeader
          allMessages={data.length}
          lastMessageTime={data[data.length - 1].createdAt}
          allUsers={this.getUsers()}
        />
        <MessageList
          data={data}
          activeUserId={activeUserId}
          prevMessageDate={data[data.length - 1].createdAt}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
        />
        <MessageInput
          onSend={this.handleSend}
          onEnter={this.handleEnter} />
        <Footer />
      </div>
    )
  }
}

export default Chat;