import React from 'react'
import '../styles/message.css';
import { convertTime } from '../helpers/functions.js'
import PropTypes from 'prop-types';

import { checkIsEarlier } from '../helpers/functions.js'
import MessageDateLine from './MessageDateLine.jsx'

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLiked: false
    };
  }

  createDateLine(prevMessageDate, newMessageDate, messageId) {
    const isEarlier = checkIsEarlier(prevMessageDate, newMessageDate);

    if (isEarlier) {
      return (
        <MessageDateLine
          newMessageDate={newMessageDate}
          messageId={messageId} />
      )
    }
    else return;
  }

  render() {
    const {
      avatar,
      text,
      date,
      messageId,
      prevMessageDate
    } = this.props;

    const onLike = () => {
      if (!this.state.isLiked) {
        document.getElementById(messageId).style.color = '#dc3939';
        let isLiked = true;
        this.setState({ isLiked })
      }
      else {
        document.getElementById(messageId).style.color = 'rgb(139, 137, 137)';
        let isLiked = false;
        this.setState({ isLiked })
      }
    }

    return (
      <div>
        {/* <div>
        {this.createDateLine(prevMessageDate,date,messageId)}
      </div> */}
        <div className="message-wrapper">
          <div className="message_item">
            <img className="message_item-avatar" src={avatar} alt="avatar" />
            <div className="message_item-text">{text}</div>
            <div className="message_item-date">{convertTime(date)}</div>
          </div>
          <div className="message_icons">
            <i className="fa fa-heart message_item-heart" id={messageId} onClick={onLike} aria-hidden="true"></i>
          </div>
        </div>
      </div>
    )
  }
}

Message.propTypes = {
  avatar: PropTypes.string,
  text: PropTypes.string,
  date: PropTypes.string,
  messageId: PropTypes.string
};

export default Message;