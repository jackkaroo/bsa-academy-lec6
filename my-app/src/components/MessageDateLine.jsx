import React from 'react'
import '../styles/message.css';

class MessageDateLine extends React.Component {

  render() {
    const {
      messageId,
      newMessageDate
    } = this.props;

    return (
      <div id={messageId}>
        {newMessageDate}
      </div>
    )
  }
}

export default MessageDateLine;