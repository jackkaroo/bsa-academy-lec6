import React from 'react'
import '../styles/message-input.css';
import PropTypes from 'prop-types';

class MessageInput extends React.Component {
  render() {
    const {
      onSend,
      onEnter
    } = this.props;

    const onClear = () => {
      onSend();
      document.getElementsByClassName('message_input-text')[0].value = ''
    }

    return (
      <div className="message_input">
        <input className="message_input-text" placeholder="Message" onChange={e => onEnter(e)} />
        <button id="message_input-button" onClick={onClear}>Send</button>
        <button id="message_edit-button">Edit</button>
      </div>
    )
  }
}

MessageInput.propTypes = {
  onSend: PropTypes.func,
  onEnter: PropTypes.func
};

export default MessageInput;