import React from 'react'
import '../styles/chat-header.css';
import { convertTime } from '../helpers/functions.js'
import PropTypes from 'prop-types';

class ChatHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Need Fix'
    }
  }

  render() {
    const {
      allMessages,
      lastMessageTime,
      allUsers
    } = this.props;

    return (
      <div className="header">
        <div className="header_item">
          <div className="header_chat-name">{this.state.name}</div>
          <div className="header_chat-users"> {allUsers.length} participants</div>
          <div className="header_chat-messages">{allMessages} messages</div>
        </div>
        <div className="header_item">
          <div className="header_chat-last">last message at {convertTime(lastMessageTime)}</div>
        </div>
      </div>
    )
  }
}

ChatHeader.propTypes = {
  lastMessageTime: PropTypes.string,
  allMessages: PropTypes.number,
  allUsers: PropTypes.array
};

export default ChatHeader;