export function convertTime(time) {
  if (time instanceof Date) {
    const newdate = time.toString().substring(16, 21);
    return newdate;
  }
  const date = new Date(time).toString();

  const newdate = date.substring(16, 21);
  return newdate;
}

export function checkIsEarlier(prevDate, newDate) {
  let prevDate1 = new Date(prevDate);
  let newDate1 = new Date(newDate);
  if (prevDate1.getFullYear() > newDate1.getFullYear())
    return true;
  if (prevDate1.getMonth() > newDate1.getMonth())
    return true;
  if (prevDate1.getDate() > newDate1.getDate()) {
    return true;
  }
  if (prevDate1.getDate() > newDate1.getDate()) {
    return true;
  }
}

export function findMessageById(data, messageId) {
  const index = data.findIndex(item => item.id === messageId)
  return index;
}

export function create_UUID() {
  let dt = new Date().getTime();
  let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}
